package com.doancaosang.gameoftrone;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.location.Location;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    /**ID de l'utilisateur*/
    private String username;
    /**Slogan*/
    private String slogan="";

    /**Bouton d'attaque*/
    private FloatingActionButton fab;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    /**Qualité minimum d'une photo pour afficher dans le jeu, pour éviter l'exception "OutOfMemory"*/
    private static final int DEFAULT_MIN_WIDTH_QUALITY = 600;        // min pixels

    /**Préfixe des fichiers stockés durables*/
    private String FILE_NAME = "com.doancaosang.gameoftrone";
    private String SUFFIXE;

    /**TAG pour les actions de l'utilisateur, mettre à jour soit la photo de profile, soit celle de sanisette*/
    private int UPDATE_PROFILE_PHOTO = 1;
    private int UPDATE_MARKER_PHOTO = 2;

    /**Les composants nécessaires pour faire fonctionner le GoogleMap*/
    private MapFragment mapFragment;
    private GoogleMap googleMap;
    private GoogleApiClient googleApiClient;
    private boolean requestingLocationUpdates = false;
    private LocationRequest locationRequest;

    /**Les intervalles de temps pour mettre à jour la location en seconde*/
    private static int UPDATE_INTERVAL = 10000; // msec
    private static int FATEST_INTERVAL = 5000; // msec
    private static int DISPLACEMENT = 0; // meters

    /**La dernière location connue de l'utilisateur*/
    private Location lastLocation;
    /**Le marqueur de l'utilisateur sur la carte*/
    private Marker lastMarker;
    /**Le cercle autour de l'utilisateur*/
    private Circle lastCircle;
    /**Rayon du cercle en mètre*/
    private static int RANGE = 5000; // meters

    /**Liste des armes*/
    private ListView listWeapons;
    /**Fragment qui contient la liste des armes*/
    private WeaponsFragment weaponsFragment;
    /**Arme utilisée actuelle*/
    private int currentWeapon;
    /**Liste des noms d'armes*/
    private String[] listWeaponsName = {"Archery", "Blade", "Bomb", "Club", "Dart",
             "Handgun", "Machine gun", "Missile", "Police stick", "Rocket", "Water gun"};
    /**Liste des sons pour chaque arme*/
    private int[] listWeaponsSounds= {R.raw.sound_arrow, R.raw.sound_blade,
            R.raw.sound_explosion, R.raw.sound_policestick,R.raw.sound_blade, R.raw.sound_gun,
            R.raw.sound_machinegun, R.raw.sound_missile, R.raw.sound_policestick,
            R.raw.sound_missile, R.raw.sound_water, R.raw.sound_explosion2};
    /**Liste des dégâts pour chaque arme*/
    private int[] listWeaponsHP = {13, 10, 27, 1, 7, 15, 20, 30, 5, 25, 2};
    /**Liste des icônes des sanisettes avant de détruire par l'utilisateur*/
    private int[] listRIdMonstersIcons = {R.raw.icon_small, R.raw.icon_small2, R.raw.icon_medium,
            R.raw.icon_medium2, R.raw.icon_medium3};
    /**Liste des icônes des sanisettes après la destruction de l'utilisateur*/
    private int[] listRIdShieldsIcons = {R.raw.icon_shield, R.raw.icon_shield2, R.raw.icon_shield3,
            R.raw.icon_shield4, R.raw.icon_shield5};
    /**Liste des icônes des sanisettes après la destruction de l'utilisateur et après la mise à jour des photos*/
    private int[] listIdColorIcons = {R.raw.icon_color, R.raw.icon_color2, R.raw.icon_color3,
            R.raw.icon_color4, R.raw.icon_color5};
    /**Liste des icônes des armes*/
    private int[] listRIdWeaponsIcons = {R.raw.icon_archery, R.raw.icon_blade, R.raw.icon_bomb,
            R.raw.icon_club, R.raw.icon_dart, R.raw.icon_handgun, R.raw.icon_machinegun,
            R.raw.icon_missile, R.raw.icon_police, R.raw.icon_rocket,R.raw.icon_watergun};

    /**Fragment de page profile*/
    private ProfileFragment profileFragment;
    /**Grande image de profile dans la page de profile*/
    private ImageFragment imageFragment;

    /**Marqueur sélecté actuel par l'utilisateur*/
    private Marker selectedMarker;

    /**Le choix de l'utilisateur actuel pour afficher la carte
     *
     *     <ul>
     *         <li> soit tous les sanisettes
     *         <li> soit que les siennes
     *         <li> soit que les autres
     *     </ul>
     * */
    private int currentSetting = 0;

    /**Liste des sanisettes libres*/
    private HashMap<String, Object[]> hashEnemy = new HashMap<>();
    /**Liste des sanisettes gagnées*/
    private HashMap<String, Object[]> hashMind = new HashMap<>();
    /**Liste des photos des sanisettes gagnées*/
    private HashMap<String, String> hashPhotos = new HashMap<>();

    /**Le chemin absolute vers le fichier photo de profile*/
    private String currentProfilePhotoPath = null;
    /**Variable signale la mise à jour de photo de profile*/
    private boolean isUpdatedCurrentProfilePath = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        username = getIntent().getStringExtra("username");
        SUFFIXE = username;

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public synchronized void onClick(View view) {
                //teste si le marqueur de sanisette a bien été choisi
                if (selectedMarker == null) {
                    MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(),
                            R.raw.sound_error);
                    mediaPlayer.setVolume(1.0f, 1.0f);
                    mediaPlayer.start();
                    Snackbar.make(view, "You have to select the enemy!", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    return;
                }

                float[] distance = new float[2];
                Location.distanceBetween(selectedMarker.getPosition().latitude,
                        selectedMarker.getPosition().longitude,
                        lastLocation.getLatitude(),
                        lastLocation.getLongitude(),
                        distance);
                //calcule si l'utilisateur peut attaquer cette sanisette
                if (distance[0] > lastCircle.getRadius()) {
                    MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(),
                            R.raw.sound_error);
                    mediaPlayer.setVolume(1.0f, 1.0f);
                    mediaPlayer.start();
                    Snackbar.make(view, "Out of your range!", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    return;
                }

                LatLng latLng = new LatLng(selectedMarker.getPosition().latitude,
                        selectedMarker.getPosition().longitude);
                Object[] o = hashEnemy.get(latLng.toString());
                //teste si ce marqueur est libre, sinon il appartient de l'utilisateur,
                // il ne peut plus l'attaquer
                if (o == null) {
                    MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(),
                            R.raw.sound_error2);
                    mediaPlayer.setVolume(1.0f, 1.0f);
                    mediaPlayer.start();
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("It's not a good idea!")
                            .setMessage("You can't attack on yourself or your places.")
                            .setPositiveButton("OK", null)
                            .show();
                    return;
                }

                Marker enemyMarker = (Marker) o[0];
                Enemy enemy = (Enemy) o[1];

                enemy.attacked(listWeaponsHP[currentWeapon]);
                Snackbar.make(view, "Attack with "
                                + listWeaponsName[currentWeapon]
                                + " -" + listWeaponsHP[currentWeapon] + "HP",
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(),
                        listWeaponsSounds[currentWeapon]);
                mediaPlayer.setVolume(1.0f, 1.0f);
                mediaPlayer.start();
                //teste si cette sanisette est encore vivante
                if (enemy.isAlive()) {
                    return;
                } else { //sinon ajoute dans la liste des sanisettes de l'utilisateur
                    selectedMarker = null;

                    mediaPlayer = MediaPlayer.create(getApplicationContext(),
                            listWeaponsSounds[listWeaponsSounds.length - 1]);
                    mediaPlayer.setVolume(1.0f, 1.0f);
                    mediaPlayer.start();

                    enemy.setOwner(username, slogan);
                    enemyMarker.setTitle(username);
                    enemyMarker.setSnippet(slogan);
                    enemyMarker.setIcon(BitmapDescriptorFactory
                            .fromBitmap(BitmapFactory
                                    .decodeResource(getResources(),
                                            listRIdShieldsIcons[enemy.typeEnemy.getID()])));

                    hashEnemy.remove(latLng.toString());
                    hashMind.put(latLng.toString(), o);
                    TextView scoreView = (TextView) findViewById(R.id.scoreView);
                    scoreView.setText(hashMind.size() + "/" + (hashEnemy.size() + hashMind.size()));

                    if (currentSetting == 2) {
                        enemyMarker.setVisible(false);
                    }

                    if (hashEnemy.size() == 0) {
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Good job!")
                                .setMessage("You are the BOSS, " + username)
                                .setPositiveButton("OK", null)
                                .show();
                    }
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.post(new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.scoreView)).setText(hashMind.size()
                        + "/" + (hashEnemy.size() + hashMind.size()));
                ((TextView) findViewById(R.id.username)).setText(username);
                ((TextView) findViewById(R.id.slogan)).setText(slogan);
                ((ImageView) findViewById(R.id.imageView)).setImageResource(R.raw.logo);
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close){
            @Override
            public void onDrawerStateChanged(int newState) {
                //teste si le menu à gauche est en train d'ouvrir
                //si oui, essaie de mettre à jour si le profile a été modifié la photo
                if (newState == DrawerLayout.STATE_SETTLING) {
                    if (currentProfilePhotoPath != null && isUpdatedCurrentProfilePath) {
                        if (isExistPath(currentProfilePhotoPath)) {
                            ImageView imageView = (ImageView) findViewById(R.id.imageView);
                            Bitmap bitmap = rotateImage(currentProfilePhotoPath, imageView.getMaxWidth(),
                                    imageView.getMaxHeight());
                            imageView.setImageBitmap(bitmap);
                        }else{
                            currentProfilePhotoPath = null;
                        }
                        isUpdatedCurrentProfilePath = false;
                    }
                //    invalidateOptionsMenu();
                }
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        listWeapons = (ListView) findViewById(R.id.listWeapons);
        weaponsFragment = (WeaponsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_weapons);
        bringWeaponsFragmentToBack();

        profileFragment = (ProfileFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_profile);
        bringProfileFragmentToBack();

        imageFragment = (ImageFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_image);

        Button profileButton = (Button) findViewById(R.id.profile_button);
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            //lorsque vous cliquez sur le button UPDATE, le clavier se cache
            public void onClick(View v) {
                InputMethodManager in =
                        (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(v.getWindowToken(), 0);
                updateProfile();
            }
        });

        TextView textView = (TextView) findViewById(R.id.profile_sloganView);
        textView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            //lorsque vous cliquez OK sur le clavier virtuel,
            // cela devient come vous cliquez sur le button UPDATE
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager in =
                            (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    updateProfile();
                    handled = true;
                }
                return handled;
            }
        });

        ImageView imageView = (ImageView) findViewById(R.id.profile_imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            //lorsque vous cliquez sur l'image de votre profile dans le fragment_profile,
            //il déclenche l'action de choisir les sources de votre nouvelle photo
            public void onClick(View v) {
                createCustomIntentChooser(UPDATE_PROFILE_PHOTO);
            }
        });

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.fragment_map);
        googleMap = mapFragment.getMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.moveCamera(CameraUpdateFactory.zoomTo(14));
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                //lorsque vous cliquez sur un marqueur de sanisette, cette méthode mémorise ce marqueur
                selectedMarker = null;
            }
        });

        getSupportFragmentManager()
                .beginTransaction()
                .hide(imageFragment)
                .commit();

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            //lorsque vous cliquez sur un marqueur
            public boolean onMarkerClick(Marker marker) {
                selectedMarker = marker;
                LatLng latLng = new LatLng(selectedMarker.getPosition().latitude,
                        selectedMarker.getPosition().longitude);
                // teste si ce marqueur appartient à l'utilisateur sinon rien ne se passe
                if (hashMind.get(latLng.toString()) == null)
                    return false;
                //ici on déclenche le Snackbar pour voir la photo ou mettre à jour la photo de sanisette,
                //qui appartient à l'utilisateur
                Snackbar.make(fab, "VIEW PHOTO", Snackbar.LENGTH_LONG)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                new AlertDialog.Builder(MainActivity.this)
                                        .setTitle("View or Update")
                                        .setMessage("You can view or update.")
                                        .setPositiveButton("UPDATE",
                                                new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (selectedMarker != null)
                                                    Log.e("test", "marker not null");
                                                createCustomIntentChooser(UPDATE_MARKER_PHOTO);
                                            }
                                        })
                                        .setNeutralButton("VIEW",
                                                new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                LatLng latLng = new LatLng(selectedMarker.getPosition().latitude,
                                                        selectedMarker.getPosition().longitude);
                                                String pathPhoto = hashPhotos.get(latLng.toString());
                                                if (pathPhoto == null)
                                                    return;
                                                if (isExistPath(pathPhoto)){
                                                    ImageView imageView1 =
                                                            (ImageView) findViewById(R.id.imageView_marker);
                                                    Bitmap bitmap = rotateImage(pathPhoto, imageView1.getMaxWidth(),
                                                            imageView1.getMaxHeight());
                                                    imageView1.setImageBitmap(bitmap);
                                                    imageView1.setOnTouchListener(new View.OnTouchListener() {
                                                        @Override
                                                        public boolean onTouch(View v, MotionEvent event) {
                                                            getSupportFragmentManager()
                                                                    .beginTransaction()
                                                                    .hide(imageFragment)
                                                                    .commit();
                                                            selectedMarker = null;
                                                            return false;
                                                        }
                                                    });
                                                    getSupportFragmentManager()
                                                            .beginTransaction()
                                                            .show(imageFragment)
                                                            .commit();
                                                }else{
                                                    new AlertDialog.Builder(MainActivity.this)
                                                            .setTitle("Error")
                                                            .setMessage("Photo is not existed!")
                                                            .setPositiveButton("OK", null)
                                                            .show();
                                                    hashPhotos.remove(latLng.toString());
                                                    Object[] o = hashMind.get(latLng.toString());
                                                    Enemy mind = (Enemy) o[1];
                                                    selectedMarker.setIcon(BitmapDescriptorFactory
                                                            .fromBitmap(BitmapFactory
                                                                    .decodeResource(getResources(),
                                                                            listRIdShieldsIcons[mind.typeEnemy.getID()]
                                                                    )));
                                                }
                                            }
                                        })
                                        .setCancelable(true)
                                        .show();
                            }
                        })
                        .show();
                return false;
            }
        });
        //teste des permissions de localisation
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Error permission")
                    .setMessage("App need permission to play!")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
            return;
        }
        //teste des services de GooglePlay
        if (checkPlayServices()) {
            buildGoogleApiClient();
            createLocationRequest();
            togglePeriodicLocationUpdates();
        }

        currentWeapon = 0;
        listWeapons.setAdapter(new CustomAdapter(this, R.layout.cell_content));
        listWeapons.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            //lorsque l'utilisateur choisit une arme
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentWeapon = position;
                ((CustomAdapter) listWeapons.getAdapter()).notifyDataSetChanged();
                Toast.makeText(MainActivity.this, listWeaponsName[position],
                        Toast.LENGTH_SHORT).show();
                bringWeaponsFragmentToBack();
                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                navigationView.setCheckedItem(R.id.nav_map);
                fab.show();
            }
        });
        //teste si il existe un sauvegarde au nom de cet utilisateur,
        //s'il existe restaurer les données
        if (!isPersist(getFilesDir().getAbsolutePath(), FILE_NAME + SUFFIXE)
                || !restorePersist(getFilesDir().getAbsolutePath(), FILE_NAME+SUFFIXE)){
            initData();
        }
    }

    /** crée un intent personalisé avec non seulement une option d'accès à la bibliothèque
     *  mais encore d'accès à la caméra
     *
     * @param type
     */
    private void createCustomIntentChooser(int type){
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        final Intent intent = new Intent(captureIntent);
        cameraIntents.add(intent);

        final Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        // choisit une option de système
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // ajoute l'option de caméra
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        startActivityForResult(chooserIntent, type);
    }

    /** teste si le chemin existe
     *
     * @param path
     * @return
     */
    private boolean isExistPath(String path){
        File file = new File(path);
        return file.exists();
    }

    /** pivote l'image vers le bon sens (le sens du téléphone lorsqu'on a pris cette image)
     *
     * @param uri
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    private Bitmap rotateImage(Uri uri, int reqWidth, int reqHeight){
        Bitmap bitmap = null;
        try {
            bitmap = getImageResized(this, uri);
            Uri tmp = getCorrectUri(uri);
            String path = FileUtility.getRealPathFromURI(getApplicationContext(),
                    tmp);
            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            Matrix matrix = new Matrix();
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                matrix.preRotate(90);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                matrix.preRotate(180);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                matrix.preRotate(270);
            }

            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true); // rotating bitmap
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /** décode un Uri avec une taille réduite selon sampleSize
     *
     * @param context
     * @param theUri
     * @param sampleSize
     * @return
     */
    private static Bitmap decodeBitmap(Context context, Uri theUri, int sampleSize) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = sampleSize;

        AssetFileDescriptor fileDescriptor = null;
        try {
            fileDescriptor = context.getContentResolver().openAssetFileDescriptor(theUri, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Bitmap actuallyUsableBitmap = BitmapFactory.decodeFileDescriptor(
                fileDescriptor.getFileDescriptor(), null, options);

        return actuallyUsableBitmap;
    }

    /**
     *  Réduit la taille d'une photo pour éviter l'exception OutOfMemory
     *
     * @param context
     * @param selectedImage
     * @return
     */

    private static Bitmap getImageResized(Context context, Uri selectedImage) {
        Bitmap bm = null;
        int[] sampleSizes = new int[]{5, 3, 2, 1};
        int i = 0;
        do {
            bm = decodeBitmap(context, selectedImage, sampleSizes[i]);
            i++;
        } while (bm.getWidth() < DEFAULT_MIN_WIDTH_QUALITY && i < sampleSizes.length);
        return bm;
    }

    /** Réduit la taille d'une photo pour éviter l'exception OutOfMemory
     *
     * @param context
     * @param path
     * @return
     */
    private static Bitmap getImageResized(Context context, String path) {
        Bitmap bm = null;
        int[] sampleSizes = new int[]{5, 3, 2, 1};
        int i = 0;
        do {
            bm = decodeBitmap(context, path, sampleSizes[i]);
            i++;
        } while (bm.getWidth() < DEFAULT_MIN_WIDTH_QUALITY && i < sampleSizes.length);
        return bm;
    }

    /**décode une photo à partir d'un chemin absolute avec une taille réduite selon sampleSize
     *
     * @param context
     * @param path
     * @param sampleSize
     * @return
     */
    private static Bitmap decodeBitmap(Context context, String path, int sampleSize) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = sampleSize;
        Bitmap actuallyUsableBitmap = BitmapFactory.decodeFile(path, options);
        return actuallyUsableBitmap;
    }

    /**pivote l'image vers le bon sens (le sens du téléphone lorsqu'on a pris cette image)
     *
     * @param path
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    private Bitmap rotateImage(String path, int reqWidth, int reqHeight){
        Bitmap bitmap = null;
        try {
            bitmap = getImageResized(this, path);
            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            Matrix matrix = new Matrix();
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                matrix.preRotate(90);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                matrix.preRotate(180);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                matrix.preRotate(270);
            }
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true); // rotating bitmap
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /** convertit un Uri en un Uri du fichier photo sur l'appareil
     *
     * @param uri
     * @return
     */
    private Uri getCorrectUri(Uri uri){
        //  /external/images/media/56837
        //  /document/image:56837
        String path = uri.getPath();
        String prefixe = "/images/media/";
        String newPath = "/document/image%3A";
        Log.e("test", "origin " + path);

        if (path.contains(prefixe)){
            int index = path.indexOf(prefixe);
            Log.e("test", path);
            Log.e("test", index + " ");
            newPath += path.substring(index + prefixe.length());
            Log.e("test", newPath);

            return Uri.parse(newPath);
        }
        return uri;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UPDATE_PROFILE_PHOTO){
            if (resultCode == Activity.RESULT_OK){
                Uri selectedImageUri = data.getData();

                currentProfilePhotoPath = FileUtility.getRealPathFromURI(getApplicationContext(),
                        getCorrectUri(selectedImageUri));
                isUpdatedCurrentProfilePath = true;

                ImageView imageView = (ImageView) findViewById(R.id.imageView);
                ImageView profile_imageView = (ImageView) findViewById(R.id.profile_imageView);

                Bitmap bitmap = rotateImage(selectedImageUri, imageView.getMaxWidth(), imageView.getMaxHeight());
                imageView.setImageBitmap(bitmap);
                bitmap = rotateImage(selectedImageUri, profile_imageView.getMaxWidth(),
                        profile_imageView.getMaxHeight());
                profile_imageView.setImageBitmap(bitmap);

                if (lastMarker != null) {
                    lastMarker.setIcon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                            .decodeResource(getResources(), R.raw.icon_me_color)));
                }
            }

        }else if (requestCode == UPDATE_MARKER_PHOTO){
            if (resultCode == RESULT_OK){
                Uri selectedImage = data.getData();
                if (selectedMarker != null){
                    Uri correctUri = getCorrectUri(selectedImage);
                    Log.e("test", correctUri.getPath());
                    String photoPath = FileUtility.getRealPathFromURI(getApplicationContext(),
                            correctUri);
                    LatLng latLng = new LatLng(selectedMarker.getPosition().latitude,
                            selectedMarker.getPosition().longitude);
                    if (hashPhotos.get(latLng.toString()) != null)
                        hashPhotos.remove(latLng.toString());
                    hashPhotos.put(latLng.toString(), photoPath);

                    Object[] o = hashMind.get(latLng.toString());
                    Enemy mind = (Enemy) o[1];
                    selectedMarker.setIcon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                            .decodeResource(getResources(), listIdColorIcons[mind.typeEnemy.getID()])));
                }
            }
        }
    }

    /** vérifie le GooglePlay Service sur l'appareil
     *
     * @return
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    /** crée un googleApiClient
     *
     */
    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_all_places) {
            currentSetting = 0;
            setAllPlacesVisibles();
            return true;
        }else if (id == R.id.action_my_places){
            currentSetting = 1;
            hideEnemyPlaces();
            return true;
        }else if (id == R.id.action_enemy_places){
            currentSetting = 2;
            hideMyPlaces();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /** cache les sanisettes libres (n'appartiennent pas à l'utilisateur)
     *
     */
    public void hideEnemyPlaces(){
        Collection<Object[]> collection = hashEnemy.values();
        for (Iterator<Object[]> iterator = collection.iterator(); iterator.hasNext();){
            Object[] o = iterator.next();
            Marker marker = (Marker) o[0];
            marker.setVisible(false);
        }
        collection = hashMind.values();
        for (Iterator<Object[]> iterator = collection.iterator(); iterator.hasNext();){
            Object[] o = iterator.next();
            Marker marker = (Marker) o[0];
            marker.setVisible(true);
        }
    }

    /** cache les sanisettes qui appartiennent à l'utilisateur
     *
     */
    public void hideMyPlaces(){
        Collection<Object[]> collection = hashEnemy.values();
        for (Iterator<Object[]> iterator = collection.iterator(); iterator.hasNext();){
            Object[] o = iterator.next();
            Marker marker = (Marker) o[0];
            marker.setVisible(true);
        }
        collection = hashMind.values();
        for (Iterator<Object[]> iterator = collection.iterator(); iterator.hasNext();){
            Object[] o = iterator.next();
            Marker marker = (Marker) o[0];
            marker.setVisible(false);
        }
    }

    /** révèle tous les sanisettes sur la carte
     *
     */
    public void setAllPlacesVisibles(){
        Collection<Object[]> collection = hashEnemy.values();
        for (Iterator<Object[]> iterator = collection.iterator(); iterator.hasNext();){
            Object[] o = iterator.next();
            Marker marker = (Marker) o[0];
            marker.setVisible(true);
        }
        collection = hashMind.values();
        for (Iterator<Object[]> iterator = collection.iterator(); iterator.hasNext();){
            Object[] o = iterator.next();
            Marker marker = (Marker) o[0];
            marker.setVisible(true);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            fab.hide();
            bringWeaponsFragmentToBack();
            bringProfileFragmentToFront();
        } else if (id == R.id.nav_map) {
            fab.show();
            bringWeaponsFragmentToBack();
            bringProfileFragmentToBack();
        } else if (id == R.id.nav_weapons) {
            fab.hide();
            bringProfileFragmentToBack();
            bringWeaponsFragmentToFront();
        } else if (id == R.id.nav_share) {
            Intent intent = createShareIntent();
            startActivity(Intent.createChooser(intent, "Share via"));
        } else if (id == R.id.nav_reset) {
            MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(),
                    R.raw.sound_error3);
            mediaPlayer.setVolume(1.0f, 1.0f);
            mediaPlayer.start(); // no need to call prepare(); create() does that for you
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Reset")
                    .setMessage("All yours places will be delete, are you sure?")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            resetData();
                            initData();
                        }
                    })
                    .setNeutralButton("CANCEL", null)
                    .setCancelable(true)
                    .show();
            ((TextView) findViewById(R.id.scoreView)).setText(hashMind.size()
                    + "/" + (hashEnemy.size() + hashMind.size()));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /** supprime tous les données de l'utilisateur actuel
     *
     */
    private void resetData(){
        Collection<Object[]> collection = hashEnemy.values();
        for (Iterator<Object[]> iterator = collection.iterator(); iterator.hasNext();){
            Object[] o = iterator.next();
            Marker marker = (Marker) o[0];
            Enemy enemy = (Enemy) o[1];
            marker.remove();
        }
        collection = hashMind.values();
        for (Iterator<Object[]> iterator = collection.iterator(); iterator.hasNext();){
            Object[] o = iterator.next();
            Marker marker = (Marker) o[0];
            Enemy enemy = (Enemy) o[1];
            marker.remove();
        }
        hashEnemy = new HashMap<>();
        hashMind = new HashMap<>();
        hashPhotos = new HashMap<>();

        currentProfilePhotoPath = null;
        ((ImageView) findViewById(R.id.profile_imageView)).setImageResource(R.raw.logo);
        ((ImageView) findViewById(R.id.imageView)).setImageResource(R.raw.logo);
        slogan = "";
        ((TextView) findViewById(R.id.slogan)).setText(slogan);
    }

    /** initialise les données
     *
     */
    private void initData(){
        hashEnemy = new HashMap<>();
        hashMind = new HashMap<>();
        hashPhotos = new HashMap<>();

        ParserFileCSV parserFileCSV = new ParserFileCSV(this, R.raw.sanisettesparis2011);
        final ArrayList<String[]> list = parserFileCSV.parseCSV();
        for (int i = 0; i < list.size(); i++){
            String[] strings = list.get(i);
            String[] stringLocation = strings[0].split(",");

            LatLng latLng = new LatLng(Double.parseDouble(stringLocation[0]),
                    Double.parseDouble(stringLocation[1]));

            Enemy enemy = new Enemy(latLng, strings[1]);
            Marker enemyMarker = googleMap.addMarker(
                    new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                    .decodeResource(getResources(),
                                            listRIdMonstersIcons[enemy.typeEnemy.getID()])))
                            .title(enemy.typeEnemy.getName())
                            .snippet(enemy.typeEnemy.getSlogan()));

            Object[] value = {enemyMarker, enemy};
            hashEnemy.put(latLng.toString(), value);
        }
    }

    /** crée un Intent de partager les points gagnés en texte
     *
     * @return
     */
    private Intent createShareIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Game of Trône");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Game Of Trône\n\nI win "+hashMind.size()
                +" of "+(hashMind.size()+hashEnemy.size())
                +" total. Would you play with me?\n\n"+username);

        return shareIntent;
    }

    /** révèle le fragment_weapons (la liste des armes) en première plan
     *
     */
    public void bringWeaponsFragmentToFront(){
        getSupportFragmentManager()
                .beginTransaction()
                .show(weaponsFragment)
                .commit();
    }

    /** met le fragment_weapons (la liste des armes) en arrière plan
     *
     */
    public void bringWeaponsFragmentToBack(){
        getSupportFragmentManager()
                .beginTransaction()
                .hide(weaponsFragment)
                .commit();
    }

    /** révèle le fragment_profile (la page de modification de profile) en première plan
     *
     */
    public void bringProfileFragmentToFront(){
        getSupportFragmentManager()
                .beginTransaction()
                .show(profileFragment)
                .commit();
    }

    /** met le fragment_profile (la page de modification de profile) en arrière plan
     *
     */
    public void bringProfileFragmentToBack(){
        getSupportFragmentManager()
                .beginTransaction()
                .hide(profileFragment)
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null)
            googleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!checkPlayServices()){
            return;
        }
        if (googleApiClient == null){
            buildGoogleApiClient();
            googleApiClient.connect();
        }

        // Resuming the periodic latLng updates
        if (googleApiClient.isConnected() && requestingLocationUpdates)
            startLocationUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!checkPlayServices()){
            return;
        }
    //    if (googleApiClient == null){
    //        buildGoogleApiClient();
    //        googleApiClient.connect();
    //    }
    //    stopLocationUpdates();
        savePersist(getFilesDir().getAbsolutePath(), FILE_NAME + SUFFIXE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (googleApiClient.isConnected())
            googleApiClient.disconnect();

        savePersist(getFilesDir().getAbsolutePath(), FILE_NAME + SUFFIXE);
    }

    @Override
    public void onConnected(Bundle bundle) {
        displayLocation();
        if (requestingLocationUpdates)
            startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i("test", "Connection failed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());
    }

    /** affiche et met à jour la position de l'utilisateur sur la carte
     *
     * */
    private void displayLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        lastLocation = LocationServices.FusedLocationApi
                .getLastLocation(googleApiClient);

        if (lastLocation != null) {
            double latitude = lastLocation.getLatitude();
            double longitude = lastLocation.getLongitude();

            LatLng tmp = new LatLng(latitude, longitude);

            if (lastMarker == null)
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(tmp));

            if (lastMarker != null)
                lastMarker.remove();

            if (currentProfilePhotoPath != null){
                lastMarker = googleMap.addMarker(
                        new MarkerOptions()
                                .position(tmp)
                                .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                        .decodeResource(getResources(), R.raw.icon_me_color)))
                                .title(username)
                                .snippet(slogan));
            }else{
                lastMarker = googleMap.addMarker(
                        new MarkerOptions()
                                .position(tmp)
                                .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                        .decodeResource(getResources(), R.raw.icon_me)))
                                .title(username)
                                .snippet(slogan));
            }

            if (lastCircle != null)
                lastCircle.remove();

            lastCircle = googleMap.addCircle(new CircleOptions()
                    .center(tmp)
                    .radius(RANGE)
                    .strokeColor(Color.RED)
                    .strokeWidth(1)
                    .fillColor(Color.argb(70, 255, 68, 68)));
        }
    }

    /**
     * méthode inutile
     * */
    private void togglePeriodicLocationUpdates() {
        requestingLocationUpdates = true;

        // Starting the latLng updates
     //   startLocationUpdates();
    }

    /** crée une requête de mettre à jour la position
     *
     * */
    protected void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FATEST_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(DISPLACEMENT); // 0 meters
    }

    /** commence de mettre à jour la position
     *
     * */
    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient, locationRequest, this);
    }

    /** arrête de mettre à jour la position
     * <p>
     * cette méthode pose plusieurs de crash donc j'ai éviter de l'utiliser.
     * Normalement, cette méthode est pour économie de l'énergie.
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                googleApiClient, this);
    }

    /**
     * Called when the latLng has changed.
     * <p> There are no restrictions on the use of the supplied Location object.
     *
     * @param location The new latLng, as a Location object.
     */
    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;
        Toast.makeText(getApplicationContext(), "Location changed!",
                Toast.LENGTH_SHORT).show();
        displayLocation();
    }

    /** Type de sanisette
     *
     */
    public enum TypeEnemy {
        WCP(10), WCG(20), WCH(30), WCH2(35), WCPC(40);

        private int hp;

        TypeEnemy(int hp){
            this.hp = hp;
        }

        int getHP(){
            return this.hp;
        }

        int getID(){
            switch (this){
                case WCP:
                    return 0;
                case WCG:
                    return 1;
                case WCH:
                    return 2;
                case WCH2:
                    return 3;
                default:
                    return 4;
            }
        }

        String getName(){
            switch (this){
                case WCP:
                    return "Free";
                case WCG:
                    return "Easy";
                case WCH:
                    return "Challenge";
                case WCH2:
                    return "Monster";
                default:
                    return "BOSS";
            }
        }

        String getSlogan(){
            switch (this){
                case WCP:
                    return "But not free";
                case WCG:
                    return "Let's do it";
                case WCH:
                    return "Come on";
                case WCH2:
                    return "Wanna try";
                default:
                    return "RUN!!!";
            }
        }
    }

    /** class Enemy qui contion la location d'un ennemi, son type, les points de vie actuels,
     * le nom de maître et son slogan avec un chemin vers la photo s'il existe
     */
    private class Enemy {
        private LatLng latLng;
        private TypeEnemy typeEnemy;
        private int heath;
        private String owner = null;
        private String slogan = null;
        private String photoPath;

        public Enemy(LatLng latLng, TypeEnemy typeEnemy){
            this.latLng = latLng;
            this.typeEnemy = typeEnemy;
            this.heath = typeEnemy.getHP();
        }

        public Enemy(LatLng latLng, String type){
            this.latLng = latLng;

            if (type.equalsIgnoreCase("WCP")){
                this.typeEnemy = TypeEnemy.WCP;
                this.heath = TypeEnemy.WCP.getHP();
            }else if (type.equalsIgnoreCase("WCG")){
                this.typeEnemy = TypeEnemy.WCG;
                this.heath = TypeEnemy.WCG.getHP();
            }else if (type.equalsIgnoreCase("WCH")){
                this.typeEnemy = TypeEnemy.WCH;
                this.heath = TypeEnemy.WCH.getHP();
            }else if (type.equalsIgnoreCase("WCH2")){
                this.typeEnemy = TypeEnemy.WCH2;
                this.heath = TypeEnemy.WCH2.getHP();
            }else{
                this.typeEnemy = TypeEnemy.WCPC;
                this.heath = TypeEnemy.WCPC.getHP();
            }
        }

        public void attacked(int damage){
            if (owner != null)
                return;
            heath -= damage;
            if (heath <= 0)
                heath = 0;
        }

        public boolean isAlive(){
            return heath > 0;
        }

        public void setHeath(int heath){
            this.heath = heath;
        }

        public int getHeath(){
            return heath;
        }

        public void setOwner(String owner, String slogan){
            if (!isAlive()){
                this.owner = owner;
                this.slogan = slogan;
            }
        }

        public String getOwner(){
            return this.owner;
        }

        public String getSlogan(){
            return this.slogan;
        }

        public LatLng getLatLng(){
            return this.latLng;
        }

        public void setPhotoPath(String photoPath){
            this.photoPath = photoPath;
        }

        public String getPhotoPath(){
            return this.photoPath;
        }
    }

    /** adapteur personalisé pour afficher des armes
     *
     */
    class CustomAdapter extends ArrayAdapter {
        private Context context;
        private int resource;

        /**
         * Constructor
         *
         * @param context  The current context.
         * @param resource The resource ID for a layout file containing a TextView to use when
         */
        public CustomAdapter(Context context, int resource) {
            super(context, resource);
            this.context = context;
            this.resource = resource;
        }

        @Override
        public int getCount() {
            return listWeaponsName.length;
        }

        @Override
        public Object getItem(int position) {
            return listWeaponsName[position];
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView = inflater.inflate(R.layout.cell_content, parent, false);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.weapon_icon);
            TextView weapon_name = (TextView) rowView.findViewById(R.id.weapon_name);
            TextView weapon_damage = (TextView) rowView.findViewById(R.id.weapon_damage);

            if (weapon_name == null)
                weapon_name = new TextView(context);

            weapon_name.setText(listWeaponsName[position]);
            weapon_name.setTextSize(40);
            weapon_name.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            if (position == currentWeapon){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    weapon_name.setTextAppearance(R.style.selectedText);
                }else{
                    weapon_name.setTextAppearance(getApplicationContext(), R.style.selectedText);
                }
                weapon_name.setAlpha((float) 1);
            }else{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    weapon_name.setTextAppearance(R.style.normalText);
                }else{
                    weapon_name.setTextAppearance(getApplicationContext(), R.style.normalText);
                }
                weapon_name.setAlpha((float) 0.5);
            }

            imageView.setImageResource(listRIdWeaponsIcons[position]);
            weapon_damage.setText(listWeaponsHP[position] +" HP");

            return rowView;
        }

    }

    /** sauvegarde des données in-game
     *
     * @param dirPath
     * @param fileName
     */
    private void savePersist(String dirPath, String fileName){
        ArrayList<Object[]> listEnemy = new ArrayList<>();
        Collection<Object[]> collections = hashEnemy.values();
        for (Iterator<Object[]> iterator = collections.iterator(); iterator.hasNext();){
            Object[] o = iterator.next();
            Enemy enemy = (Enemy) o[1];
            Object[] saveObject = {enemy.getLatLng().latitude, enemy.getLatLng().longitude,
                enemy.typeEnemy.toString(), enemy.getHeath()};
            listEnemy.add(saveObject);
        }

        ArrayList<Object[]> listMind = new ArrayList<>();
        collections = hashMind.values();
        for (Iterator<Object[]> iterator = collections.iterator(); iterator.hasNext();){
            Object[] o = iterator.next();
            Enemy enemy = (Enemy) o[1];
            Object[] saveObject = {enemy.getLatLng().latitude, enemy.getLatLng().longitude,
                    enemy.typeEnemy.toString()};
            listMind.add(saveObject);
        }
        Object[] o = {username, slogan, listEnemy, listMind, currentWeapon,
                currentProfilePhotoPath, hashPhotos};

        this.save(o, dirPath, fileName);
    }

    /** restaure des données sauvegardées in-game
     *
     * @param dirPath
     * @param fileName
     * @return
     */
    private boolean restorePersist(String dirPath, String fileName){
        Object[] o = (Object[]) this.restore(dirPath, fileName);
        if (o == null){
            return false;
        }

        username = (String) o[0];
        slogan = (String) o[1];
        currentWeapon = (int) o[4];
        ((CustomAdapter) listWeapons.getAdapter()).notifyDataSetChanged();

        hashPhotos = (HashMap<String, String>) o[6];

        currentProfilePhotoPath = (String) o[5];
        if (currentProfilePhotoPath != null) {
            if (isExistPath(currentProfilePhotoPath)) {
                isUpdatedCurrentProfilePath = true;
                ImageView imageView = (ImageView) findViewById(R.id.profile_imageView);
                Bitmap bitmap = rotateImage(currentProfilePhotoPath, imageView.getMaxWidth(),
                        imageView.getMaxHeight());

                imageView.setImageBitmap(bitmap);

                if (lastMarker != null)
                     lastMarker.setIcon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                        .decodeResource(getResources(), R.raw.icon_me_color)));
            }else{
                currentProfilePhotoPath = null;
            }
        }

        ArrayList<Object[]> listEnemy = (ArrayList<Object[]>) o[2];
        ArrayList<Object[]> listMind = (ArrayList<Object[]>) o[3];

        for (int i = 0; i < listEnemy.size(); i++){
            Object[] saveObject = listEnemy.get(i);
            LatLng latLng = new LatLng((double)saveObject[0], (double)saveObject[1]);
            Enemy enemy = new Enemy(latLng, (String) saveObject[2]);
            enemy.setHeath((int) saveObject[3]);
            Marker enemyMarker = googleMap.addMarker(
                    new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                    .decodeResource(getResources(),
                                            listRIdMonstersIcons[enemy.typeEnemy.getID()])))
                            .title(enemy.typeEnemy.getName())
                            .snippet(enemy.typeEnemy.getSlogan()));

            Object[] value = {enemyMarker, enemy};
            hashEnemy.put(latLng.toString(), value);
        }

        for (int i = 0; i < listMind.size(); i++){
            Object[] saveObject = listMind.get(i);
            LatLng latLng = new LatLng((double)saveObject[0], (double)saveObject[1]);
            Enemy mind = new Enemy(latLng, (String) saveObject[2]);
            mind.setHeath(0);
            Marker mindMarker = googleMap.addMarker(
                    new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory
                                    .fromBitmap(BitmapFactory
                                            .decodeResource(getResources(),
                                                    listRIdShieldsIcons[mind.typeEnemy.getID()])))
                            .title(username)
                            .snippet(slogan));

            Object[] value = {mindMarker, mind};
            hashMind.put(latLng.toString(), value);
            if (hashPhotos.get(latLng.toString())!=null)
                mindMarker.setIcon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                    .decodeResource(getResources(), listIdColorIcons[mind.typeEnemy.getID()])));
        }

        return true;
    }

    /** teste si une donnée existe sur le téléphone
     *
     * @param dirPath
     * @param fileName
     * @return
     */
    private boolean isPersist(String dirPath, String fileName){
        File file = new File(dirPath, fileName);
        File dir = getCacheDir();

        if (file.exists())
            return true;
        return false;
    }

    /** méthode d'écriture sur un fichier. Il faut tester s'il existe le fichier.
     *
     * @param in
     * @param dirPath
     * @param fileName
     */
    private void save(Object in, String dirPath, String fileName) {
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(in);
            os.close();
            fos.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /** méthode de lecture d'un fichier. Il faut tester s'il existe le fichier.
     *
     * @param dirPath
     * @param fileName
     * @return
     */
    private Object restore(String dirPath, String fileName){
        FileInputStream fis = null;
        try {
            fis = openFileInput(fileName);
            ObjectInputStream is = new ObjectInputStream(fis);
            Object out = is.readObject();
            is.close();
            fis.close();
            return out;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /** met à jour le profile
     *
     */
    private void updateProfile(){
        TextView textView = (TextView) findViewById(R.id.profile_sloganView);
        if (textView.getText() != null)
            slogan = textView.getText().toString();
        ((TextView) findViewById(R.id.slogan)).setText(slogan);

        Collection<Object[]> myPlaces = hashMind.values();
        for (Iterator<Object[]> iterator = myPlaces.iterator(); iterator.hasNext();){
            Object[] o = iterator.next();
            Marker marker = (Marker) o[0];
            Enemy enemy = (Enemy) o[1];
            marker.setTitle(username);
            marker.setSnippet(slogan);
            enemy.setOwner(username, slogan);
        }
        Toast.makeText(MainActivity.this, "Updated", Toast.LENGTH_SHORT).show();
        synchronized (lastMarker) {
            lastMarker.setTitle(username);
            lastMarker.setSnippet(slogan);
        }
        bringProfileFragmentToBack();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nav_map);
        fab.show();
    }

}
