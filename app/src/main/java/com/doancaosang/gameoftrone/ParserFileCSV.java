package com.doancaosang.gameoftrone;

import android.content.Context;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Created by dcs on 07/01/16.
 */
public class ParserFileCSV {
    private Context context;
    private int resourceId;

    public ParserFileCSV(Context context, int resourceId){
        this.context = context;
        this.resourceId = resourceId;
    }

    public ArrayList<String[]> parseCSV(){
        String next[] = {};
        ArrayList<String[]> list = new ArrayList<String[]>();

        try {
            InputStreamReader isr = new InputStreamReader(
                    context.getResources().openRawResource(resourceId));
            CSVReader reader = new CSVReader(isr, ';','"',1);
            while(true) {
                next = reader.readNext();
                if(next != null) {
                    list.add(next);
                } else {
                    break;
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }
}
