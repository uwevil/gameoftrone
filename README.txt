Mon projet contient 7 fichiers de Java dont il y a 2 fichiers FileUtility pour traiter et traduire le lien Uri à un chemin réel sur l'appareil, ParserFileCSV pour parser le fichier CSV des sanisettes.

LoginActivity passe Intent vers MainActivity si l'utilisateur entre son pseudo. MainActivity contient 4 fragments: MapFragment (pour la carte), ProfileFragment (pour modifier le profile de l'utilisateur, le photo), WeaponsFragment (pour choisir les armes, c'est une liste) et ImageFragment pour afficher les photos des sanisettes capturées par l'utilisateur.

J'ai implémenté les options comme:
- persistence durable: lorsque vous tapez votre pseudo, l'application retient votre pseudo pour la prochaine visite, si vous entrez à nouveau votre pseudo qui est différent à l'ancien, vous êtes identifiés comme le nouveau utilisateur. Quand vous revient à votre ancien pseudo en tapant le même pseudo, vos données sont restaurées. Vos points, vos photos personnalisées sont retenues sur la mémoire interne, elles restent jusqu'à quand vous supprimez le jeux. Pour chaque pseudo différent, les données stockées sont différentes.
- gestion de la rotation
- partage du score: sur le menu à gauche, vous choisissez "Share" pour partager un morceau du texte.
- affichage personnalisé de la carte: bouton en haut à droite.
- liste des armes personnalisées: liste avec les icons, le nom d'arme est en gras si elle est choisie.
- ajout d'une musique: pour chaque type d'arme, bruit de notification si vous faites des choses interdites comme attaquez votre sanisette ou vous même...
- changement de la photo de profile: vous se dirige vers la section profile en tapant le menu en haut à gauche, touchez la photo affichée et choissisez la source pour changer soit dans votre bibliothèque de votre téléphone soit par la caméra.
- prise en photo des sites: lorsque vous cliquez sur une sanisette que vous avez capturée vous voyez en bas un snackbar qui vous demande de voir la photo, ensuite vous avez 2 choix: voir la photo ou mettre à jour la photo.

Les difficultés: 
- le google service crash plusieurs fois lorsqu'on prend une photo et revient à l'application, ce problème j'ai aucune solution.
- lorsque vous prennez une photo à partir de la caméra, c'est compliqué à traduire Uri vers le chemin sur l'application, car ExifInterface ne marche pas avec Uri out Uri.toString() (ExifInterface pour gérer la bonne rotation de la photo capturée par la caméra). La gestion des liens Uri et des chemins sur l'appareil est compliquée et surtout il faut gérer sur des versions API différentes.

Note: 
- dans l'application j'ai choisi une range effective 5km pour tester car autour de chez moi il n'y a pas de sanisette.
- j'ai testé avec mon téléphone qui fonctionne sur API 22 Lollipop

Merci.